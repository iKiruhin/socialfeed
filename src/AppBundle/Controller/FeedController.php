<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class made to manage feed of posts
 * @Route("/")
 */
class FeedController extends Controller
{
    /**
     * @Route("/", name="home_page")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return $this->getPostedItems($request);
    }

    /**
     * @Route("/newPosts", name="get_new_posts")
     * @Template("AppBundle:Feed:getNewPosts.xml.twig")
     * @param Request $request
     * @Method("POST")
     * @return array
     */
    public function getNewPostsAction(Request $request)
    {
        $lastReceivedPostId = $request->request->get('lastReceivedPostId');
        //todo: validate $lastReceivedPostId

        $entities = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->getNewPostsResult($lastReceivedPostId);

        if (!empty($entities)) {
            $lastReceivedPostId = reset($entities)->getId();
        }

        return array(
            'entities' => $entities,
            'lastReceivedPostId' => $lastReceivedPostId,
        );
    }

    /**
     * @Route("/like", name="register_like")
     * @param Request $request
     * @Method("POST")
     * @return Response
     */
    public function registerLikeAction(Request $request)
    {
        $entityId = $request->request->get('entityId');
        //todo: validate $entityId
        return $this->changeEntityLikeState($entityId, true);
    }

    /**
     * @Route("/dislike", name="register_dislike")
     * @param Request $request
     * @Method("POST")
     * @return Response
     */
    public function registerDislikeAction(Request $request)
    {
        $entityId = $request->request->get('entityId');
        //todo: validate $entityId
        return $this->changeEntityLikeState($entityId, false);
    }

    /**
     * @Route("/addPost", name="add_post")
     * @param Request $request
     * @Method("POST")
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addPostAction(Request $request)
    {
        $postAuthor = $request->request->get('postAuthor');
        $postTitle = $request->request->get('postTitle');
        $postBrief = $request->request->get('postBrief');
        $postContent = $request->request->get('postContent');
        //todo: validate post fields

        $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->addPost($postAuthor, $postTitle, $postBrief, $postContent);

        return $this->ResponseJson200();
    }

    /**
     * @Route("/createNewPost.js", name="create-new-post-js")
     * @param Request $request
     * @Method("GET")
     * @return Response
     */
    public function newPostJsAction(Request $request)
    {
        $rendered = $this->renderView('AppBundle:Feed:createNewPost.js.twig');
        $response = new Response($rendered);
        $response->headers->set('Content-Type', 'text/javascript');
        return $response;
    }


    /**
     * @param $request
     * @return array
     */
    private function getPostedItems($request)
    {
        $entities = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post')->getAllActivePosts();

        $lastReceivedPostId = 0;
        if (!empty($entities)) {
            $lastReceivedPostId = reset($entities)->getId();
        }

        return array(
            'entities' => $entities,
            'lastReceivedPostId' => $lastReceivedPostId,
        );
    }

    /**
     * @param $entityId
     * @param $likeState
     * @return Response
     */
    private function changeEntityLikeState($entityId, $likeState)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $entity Post */
        $entity = $em->getRepository('AppBundle:Post')->find($entityId);
        if (!$entity) {
            throw $this->createNotFoundException(
                'No entity found for id ' . $entityId
            );
        }
        $entity->setIsLiked($likeState);
        $em->flush();

        return $this->ResponseJson200();
    }

    /**
     * @return Response
     */
    public function ResponseJson200(): Response
    {
        $response = array("code" => 200, "success" => true);
        //return result as JSON
        return new Response(json_encode($response));
    }
}