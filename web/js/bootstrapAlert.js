bootstrapAlert = function () {
};

bootstrapAlert.success = function (message) {
    //todo: extract same markup to separate function
    $('#alert_placeholder').prepend('<div class="alert alert-success alert-dismissible"><a class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>')
};

bootstrapAlert.danger = function (message) {
    $('#alert_placeholder').prepend('<div class="alert alert-danger alert-dismissible"><a class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>')
};