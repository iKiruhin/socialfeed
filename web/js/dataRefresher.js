function DataRefresher() {
}

DataRefresher.prototype.SetAutorefresh = function (containerSelector, period, sourceUrl, completeHandler) {
    this.constructor.SetAutorefresh(containerSelector, period, sourceUrl, completeHandler);
};

DataRefresher.SetAutorefresh = function (containerSelector, period, sourceUrl, completeHandler) {
    var container = $(containerSelector);

    //remove CDATA, aka <![CDATA[ {{xxx}} ]]> -> {{xxx}}
    function getRidOfCdata(content) {
        return content.substr(9, content.length - 12); // todo: get rid of black magic with numbers
    }

    // refresh every x seconds
    setInterval(function () {
        var data = {'lastReceivedPostId': lastReceivedPostId};
        $.ajax({
            url: sourceUrl,
            data: data,
            type: "POST",
            success: function (newItemsXml) {
                var xml = newItemsXml,
                    xmlDoc = $.parseXML(xml),
                    $xml = $(xmlDoc),
                    itemsHtmlDom = getRidOfCdata($xml.find("htmlDom").html().trim());
                var _lastReceivedPostId = parseInt($xml.find("lastReceivedPostId").text().trim());
                if (_lastReceivedPostId > lastReceivedPostId) {
                    container.prepend(itemsHtmlDom);
                    lastReceivedPostId = _lastReceivedPostId;
                    completeHandler();
                }
            },
            error: function (json) {
                console.log(json);
                throw "Error occurred at the server side during check for new posts";
            }
        });
    }, period);
};