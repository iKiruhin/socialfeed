Task:
(HTML/CSS/JS/PHP (Symfony)
- Front end - Responsive web-app with feed containing posts that can be �liked'.
- Back end - Simple API to control the information for the front-end. Feed data should be pulled from an external source (MySQL DB)

To run the project deploy first the database using script social_feed.sql
And then run console command from the root solution folder:
"php app/console server:run"

Linked Trello board: https://trello.com/b/z1jSp26H/socialfeed

.checkout
=========

A Symfony project created on November 29, 2017, 1:07 pm.
